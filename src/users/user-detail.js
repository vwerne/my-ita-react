import React from 'react';
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import usersService from './users-service'

class UserDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {}
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id)
    {
      this.load(newProps.match.params.id)
    }
  }

  async load(id) {
    const res = await usersService.getUser(id)

    this.setState({
      user: res.data
    })
  }
  
  render() {
      const user = this.state.user

      return (
        <div>
          <h2>Detail</h2>
        
          <div className="btn-group" role="group" aria-label="Basic example">
            <Link to={ROUTES.getUrl(ROUTES.USER_EDIT, { id: user.id })} className="btn btn-light">Edit</Link>
            <Link to={ROUTES.getUrl(ROUTES.USER_LISTING)} className="btn btn-danger">Delete</Link>
          </div>
        
          <table>
            <tbody>
              <tr>
                <th>Login</th>
                <td>{user.login}</td>
              </tr>
              <tr>
                <th>Name</th>
                <td>{user.name}</td>
              </tr>
            </tbody>
          </table>          
      </div>
      )
  }
}


export default UserDetail