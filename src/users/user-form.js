import React from 'react'

const UserForm = ({ user }) => {
    const handleChange = (e) =>
    user[e.target.name] = e.target.value

    return(
        <form>
            <div className="form-row">
                <div className="form-group col-md-4">
                    <label>Login</label>
                    <input name="login" type="email" className="form-control" id="login" placeholder="Login" value={user.login} onChange={handleChange}/>
                </div>
                <div className="form-group col-md-4">
                    <label>Name</label>
                    <input name="name" type="name" className="form-control" id="name" placeholder="Name" value={user.name} onChange={handleChange}/>
                </div>
            </div>
        </form>
    )
}

export default UserForm