import React from 'react';

import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

class UserListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      users: []
    }
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    const res = await usersService.getUsers()

    this.setState({
      users: res.data
    })
  }

  render() {
    const users = this.state.users

    return (
      <div>
        <table className="table">
        <thead>
          <tr>
            <th scope="col">Login</th>
            <th scope="col">Name</th>
          </tr>
        </thead>
        <tbody>
          {users.map(u =>
            <tr key={u.id}>
              <td>
                <Link to={ROUTES.getUrl(ROUTES.USER_DETAIL, {id: u.id})}>{u.login}</Link>
              </td>
              <td>{u.name}</td>
            </tr>
          )}
        </tbody>
        </table>
        <div>
          <Link to={ROUTES.USER_NEW} className="btn btn-light">Create New</Link>
        </div>
      </div>
    )
  }
}

export default UserListing;