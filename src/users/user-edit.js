import React from 'react'

import { Link } from 'react-router-dom'
 
import ROUTES from '../routes'
import usersService from './users-service'
import UserForm from './user-form'


class UserEdit extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            user: null
        }
    }

    componentWillMount() {
        this.load(this.props.match.params.id)
    }
    
    componentWillReceiveProps(newProps) {
        if (this.props.match.params.id !== newProps.match.params.id)
        {
          this.load(newProps.match.params.id)
        }
    }
    
    load(id) {
        usersService.getUser(id).then(res => {
          this.setState({
            user: res.data
          })
        })
    }    
    
    update() {
        usersService.updateUser(this.state.user).then(res => {
           this.props.history.push(ROUTES.USER_LISTING)
        })
    }  
    
    render() {
        const user = this.state.user
        
        return (
          <div>
            <h2>Edit</h2>
            {user &&  <UserForm user={user} />}
            <button className="btn btn-primary" onClick={() => this.update()}>Submit</button>
          </div>
        )  
    }   
}

export default UserEdit