export default {
    CONTACT_LISTING: "/",
    CONTACT_NEW: "/new",
    CONTACT_DETAIL: "/detail/:id",
    CONTACT_EDIT: "/edit/:id",

    USER_LISTING: "/users",
    USER_EDIT: "/users/edit/:id",
    USER_DETAIL: "/users/detail/:id",
    USER_NEW: "/users/new",

    getUrl(path, params = {}) {
        return path.replace(/:(\w+)/g, (m, k) => params[k])
    }
}