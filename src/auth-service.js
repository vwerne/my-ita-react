import axios from 'axios'

export default {
    user: null,

    onChange: null,

    async init() {
        this.reload()
    },

    async reload() {
        try {
            const res = await axios.get('http://localhost:3000/user')
            this.user = res.data
        } catch (e) {
            this.user = null
        }
        
        this.onChange()
    },

    isLoggedIn() {
        return this.user !== null
    },

    async login() {
        await axios.post('http://localhost:3000/login')
        this.reload()
    },

    async logout() {
        await axios.post('http://localhost:3000/logout')
        this.reload()
    }
}