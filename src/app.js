import React from 'react';
import { HashRouter, Route, Switch, NavLink } from 'react-router-dom';

import Login from './login'

import UserListing from './users/user-listing';
import UserEdit from './users/user-edit';
import UserDetail from './users/user-detail';
import UserNew from './users/user-new';

import ContactListing from './contacts/contact-listing'
import ContactDetail from './contacts/contact-detail'
import ContactEdit from './contacts/contact-edit'
import ContactNew from './contacts/contact-new'
import authService from './auth-service'

import ROUTES from './routes'


const App = () =>
  <HashRouter>
    <div>
      {(!authService.isLoggedIn()) && <Login />}

      {authService.isLoggedIn() && <div>
        <Navigation brand="ContactsApp"/>

        <div className="container-fluid">
        <Switch>
            <Route path={ROUTES.USER_EDIT} component={UserEdit}/>
            <Route path={ROUTES.USER_DETAIL} component={UserDetail}/>
            <Route path={ROUTES.USER_NEW} component={UserNew}/>
            <Route path={ROUTES.USER_LISTING} component={UserListing}/>
            <Route path={ROUTES.CONTACT_DETAIL} component={ContactDetail}/>
            <Route path={ROUTES.CONTACT_NEW} component={ContactNew}/>
            <Route path={ROUTES.CONTACT_EDIT} component={ContactEdit}/>
            <Route path={ROUTES.CONTACT_LISTING} component={ContactListing}/>
          </Switch>
        </div>
      </div>}
    </div>
  </HashRouter>

const getBrandText = () => {
  return "MyApp" + Date.now()
}
  

const Navigation = () =>
  <nav className="navbar navbar-light bg-light navbar-expand">

    <ul className="navbar-nav">
      <li className="nav-item">
        <NavLink exact className="nav-link" to={ROUTES.CONTACT_LISTING}>Contacts</NavLink>
      </li>

      <li className="nav-item">
        <NavLink exact className="nav-link" to={ROUTES.USER_LISTING}>Users</NavLink>
      </li>

      <li className="nav-item">
        <a className="nav-link" onClick={() => authService.logout()}>Logout</a>
      </li>
    </ul>
  </nav>

export default App;
