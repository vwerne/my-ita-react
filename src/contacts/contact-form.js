import React from 'react';

const ContactForm = ({ contact }) => {
    const handleChange = (e) =>
    contact[e.target.name] = e.target.value
  
    return (
      <form>
        <div className="form-row">
          <div className="form-group col-md-4">
            <label>Name</label>
            <input name="name" type="name" className="form-control" id="name" placeholder="Name" value={contact.name} onChange={handleChange}/>
          </div>
          <div className="form-group col-md-4">
            <label>Email address</label>
            <input name="email" type="email" className="form-control" id="email" placeholder="Email" value={contact.email} onChange={handleChange}/>
          </div>
          <div className="form-group col-md-4">
            <label>Phone</label>
            <input name="phone" type="text" className="form-control" id="phone" placeholder="Phone" value={contact.phone} onChange={handleChange}/>
          </div>
          <div className="form-group">
          <label>City</label>
          <input name="city" type="text" className="form-control" id="city" placeholder="City" value={contact.city} onChange={handleChange}/>
        </div>
        </div>
        <div className="form-group">
          <label>Note</label>
          <input name="note" type="text" className="form-control" id="note" placeholder="Note" value={contact.note} onChange={handleChange}/>
        </div>
      </form>
    )
  }

  export default ContactForm