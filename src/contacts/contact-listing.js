import React from 'react';

import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import contactsService from './contacts-service'

class ContactListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contacts: [],
      searchString: ""
    }
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    const res = await contactsService.getContacts()
      
    this.setState({
      contacts: res.data
    })
  }

  async updateSearchString(e) {
    const res = await this.setState({searchString: e.target.value})

    this.loadFilteredContactList()

  } 

  async loadFilteredContactList() {
    const res = await contactsService.searchContacts(this.state.searchString)
      
    this.setState({
      contacts: res.data
    })
  }

  render() {
    const contacts = this.state.contacts

    return (
      <div>
        <input className="form-control float-right mb-2 mt-2" style={{"width" : "200px"}} placeholder="Search" value={this.state.searchString} onChange={e => this.updateSearchString(e)} />
        <table className="table">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Phone</th>
            <th scope="col">E-mail</th>
            <th scope="col">City</th>
            <th scope="col">Note</th>
          </tr>
        </thead>
        <tbody>
          {contacts.map(c =>
            <tr key={c.id}>
              <td>
                <Link to={ROUTES.getUrl(ROUTES.CONTACT_DETAIL, {id: c.id})}>{c.name}</Link>
              </td>
              <td>{c.phone}</td>
              <td>{c.email}</td>
              <td>{c.city}</td>
              <td>{('' + c.note).slice(0, 100)}</td>
            </tr>
          )}
        </tbody>
        </table>
        <div>
          <Link to={ROUTES.CONTACT_NEW} className="btn btn-light">Create New</Link>
        </div>
      </div>
    )
  }
}

export default ContactListing;