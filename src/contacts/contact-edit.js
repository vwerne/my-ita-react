import React from 'react';
import { Link } from 'react-router-dom'
 
import ROUTES from '../routes'
import contactsService from './contacts-service'
import ContactForm from './contact-form'


class ContactEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contact: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id)
    {
      this.load(newProps.match.params.id)
    }
  }

  load(id) {
    contactsService.getContact(id).then(res => {
      this.setState({
        contact: res.data
      })
    })
  }

  update() {
    contactsService.updateContact(this.state.contact).then(res => {
       this.props.history.push(ROUTES.CONTACT_LISTING)
    })
  }

  render() {
    const contact = this.state.contact
    
    return (
      <div>
        <h2>Edit</h2>
        {contact &&  <ContactForm contact={contact} />}
        <button className="btn btn-primary" onClick={() => this.update()}>Submit</button>
      </div>
    )  
  }
}

export default ContactEdit